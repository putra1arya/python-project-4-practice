import time

start_time = time.time()

print('Hello')
print('World')
print('hello world')

# ini adalah comment

a = 10 # ini adalah comment juga
"""ada apa dengan ucup dan otong di ganteng dalam comment multiline (ini untuk comment multiline)"""

print (a)
print(time.time() - start_time, "detik")

#kita bisa mengcompile python ke yang namanya bytecode
# cara mengcompile, buka terminal dan tuliskan python -m py_compile Main.py
# maka akan dibuatkan folder baru dengan nama __pycache__
# program yang telah di compile akan dijalankan lebih cepat dibanding yang belum. Akan tetapi ketika ada perubahan harus di compile lagi

