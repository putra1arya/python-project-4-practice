# latihan konversi satuan temperature
# program konversi suhu

def exit(val) :
    if val.lower() in {'q', 'quit', 'e', 'exit'} :
            print("Goodbye!")
            start()
def start() :
    print('\n PROGRAM KONVERSI TEMPERATUR')
    print('1. Celcius')
    print('2. Reamur')
    print('3. Fahrenheit')
    print('4. Kelvin')
    while True:
        tempAwal = input('Masukkan Satuan Temperatur Input:')
        exit(tempAwal)
        tempAkhir = input('Masukkan Satuan Temperatur Output:')
        exit(tempAkhir)
        suhu = float(input('Masukkan Suhu dalam Satuan Input : '))
        if tempAwal.lower() == "celcius":
            if tempAkhir.lower() == "reamur":
                hasil = 4 / 5 * suhu
                print(" Hasil Konversi dari",tempAwal," ke ", tempAkhir, " adalah " ,hasil,tempAkhir)
            elif tempAkhir.lower() == "fahrenheit":
                hasil = 9 / 5 * suhu + 32
                print(" Hasil Konversi dari",tempAwal," ke ", tempAkhir, " adalah " ,hasil,tempAkhir)
            elif tempAkhir.lower() == 'kelvin':
                hasil = 273 + suhu
                print(" Hasil Konversi dari",tempAwal," ke ", tempAkhir, " adalah " ,hasil,tempAkhir)
            else :
                print("Satuan Temperatur Tidak Ditemukan!")
                start()
                
        elif tempAwal.lower() == "reamur":
            if tempAkhir.lower() == "celcius":
                hasil = 5/4 * suhu
                print(" Hasil Konversi dari",tempAwal," ke ", tempAkhir, " adalah " ,hasil,tempAkhir)
            elif tempAkhir.lower() == "fahrenheit":
                hasil = 9 / 4 * suhu + 32
                print(" Hasil Konversi dari",tempAwal," ke ", tempAkhir, " adalah " ,hasil,tempAkhir)
            elif tempAkhir.lower() == 'kelvin':
                hasil = 5/4 * suhu + 273
                print(" Hasil Konversi dari",tempAwal," ke ", tempAkhir, " adalah " ,hasil,tempAkhir)
            else :
                print("Satuan Temperatur Tidak Ditemukan!")
                start()
        elif tempAwal.lower() == "fahrenheit":
            if tempAkhir.lower() == "celcius":
                hasil = 5/9 * (suhu-32)
                print(" Hasil Konversi dari",tempAwal," ke ", tempAkhir, " adalah " ,hasil,tempAkhir)
            elif tempAkhir.lower() == "reamur":
                hasil = 4/9 * (suhu - 32)
                print(" Hasil Konversi dari",tempAwal," ke ", tempAkhir, " adalah " ,hasil,tempAkhir)
            elif tempAkhir.lower() == 'kelvin':
                hasil = (1 - 32 ) * 5/9 + 273
                print(" Hasil Konversi dari",tempAwal," ke ", tempAkhir, " adalah " ,hasil,tempAkhir)
            else :
                print("Satuan Temperatur Tidak Ditemukan!")
                start()
        elif tempAwal.lower() == "kelvin":
            if tempAkhir.lower() == "celcius":
                hasil = suhu - 273
                print(" Hasil Konversi dari %a ke %b adalah %c %d", tempAwal,tempAkhir,hasil,tempAkhir)
            elif tempAkhir.lower() == "reamur":
                hasil = 4/5 * (suhu - 273)
                print(" Hasil Konversi dari %a ke %b adalah %c %d", tempAwal,tempAkhir,hasil,tempAkhir)
            elif tempAkhir.lower() == 'fahrenheit':
                hasil = (1 - 273 ) * 9/5 + 32
                print(" Hasil Konversi dari %a ke %b adalah %c %d", tempAwal,tempAkhir,hasil,tempAkhir)
            else :
                print("Satuan Temperatur Tidak Ditemukan!")
                start()
        
        else:
            print("Satuan Suhu Tidak Ditemukan!")
            start()
            
start()