def main(userInput):
    arr = [65,69,3,24,11,12,51,23,57,24,35]
    arr.sort()
    
    low = 0
    high = len(arr) - 1
    
    while low <= high:
        mid = (low + high) // 2 #floor the decimal
        guess = arr[mid]
        if guess == userInput :
            return mid
        elif guess < low :
            low = mid + 1
        else :
            high = mid - 1
    
    return -1

userInput = int(input("Input Your Number : "))
result = main(userInput)

if result != -1:
    print(f"{userInput} found at index {result}")
else:
    print(f"{userInput} Not Found")
        
    
