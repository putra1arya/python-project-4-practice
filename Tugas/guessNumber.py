import random 

def getNumberRight(user,myNum) :
    diff = abs(int(myNum) - int(user))
    if diff > 50 :
        return 'YOU ARE FREEZING! Your Number Difference More Than 50'
    if diff > 40 :
        return 'You Getting Numb! Your Number Difference More Than 40'
    if diff > 30 :
        return 'You Feel the Cold! Your Number Difference More Than 30'
    if diff > 20 :
        return 'You Feel Warmer! Your Number Difference More Than 20'
    if diff > 10 :
        return 'You Feel Like in Summer! Your Number Difference More Than 10'
    if diff > 5 :
        return 'You Feel Scorcing Hot! Your Number Difference More Than 5'
    elif int(user) != myNum :
        return 'You Burning! Your Number Difference Less Than 5'
    else :
        return 'You Got It Right! IT IS ' + str(myNum)

def checkUserInput(user):
    try:
        val = int(user)
        return True
    except ValueError:
        try:
            val = float(user)
            return True
        except ValueError:
            return False
        
myNumber = random.randint(1,100)
print('Hey Lets Play Guess My Number. I will take a random number from 1 to 100. Guess My Number 10 attempt')
user = 0
attempt = 10
while not user or not checkUserInput(user) or user == 0 or int(user) != myNumber:
    if(attempt == 0) :
        print("You Lose The Number is " + str(myNumber))
        break
    user = input('Guess My Number :')
    if not user or not checkUserInput(user):
        print("You Have To Input a Number")
        continue
    print(getNumberRight(user,myNumber))
    if(int(user) == myNumber):
        break
    attempt -= 1
    