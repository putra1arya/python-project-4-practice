from pathlib import Path
import random

def checkInput(userInput,word,wordNow,arrCorrectWord):
    message = ''
    error = 0
    correct = 0
    if userInput in arrCorrectWord :
        message = 'You already guess this character! Try another!'
        return message, correct, error,wordNow,arrCorrectWord
    if(len(userInput) != 1) :
        message = 'You can only input 1 character each turn'
        return message, correct, error,wordNow,arrCorrectWord
    if userInput in word :
        for idx,i in enumerate(word) :
            if userInput == i :
                correct += 1
                wordNow[idx] = userInput
                if userInput not in arrCorrectWord:
                    arrCorrectWord.append(userInput)
        message = "You Got a Correct Character"
        return message, correct, error,wordNow,arrCorrectWord
    message = "You Got Wrong Character"
    error = 1
    return message, correct, error,wordNow,arrCorrectWord
    
            
    
HANGMAN_PICS = ['''
   +---+
       |
       |
       |
      ===''', '''
   +---+
   O   |
       |
       |
      ===''', '''
   +---+
   O   |
   |   |
       |
      ===''', '''
   +---+
   O   |
  /|   |
       |
      ===''', '''
   +---+
   O   |
  /|\  |
       |
      ===''', '''
   +---+
   O   |
  /|\  |
  /    |
      ===''', '''
   +---+
   O   |
  /|\  |
  / \  |
      ===''']
myPath = Path.cwd()
myFile = open(str(myPath) + '/Tugas/src/words.txt','r')
arrWords = myFile.read().split('\n')
print('Lets Play Hangman Game!')
randomIndex = random.randrange(len(arrWords))
myWord = arrWords[randomIndex]
myCorrect = 0
myError = 0
word = []
arrCorrectWord = []
for i in myWord:
    word.append('_')
while myError != 6 or myCorrect != len(myWord) :
    correctWord = ''.join(str(i) for i in word)
    print(correctWord)
    print(HANGMAN_PICS[myError])
    if(myError == 6 ) :
        print('  H A N G M A N')
        print('G A M E  O V E R')
        break
    if(myCorrect == len(myWord)):
        print('H A N G M A N')
        print('Y O U  W O N')
        break
    userInput = input('Input Your Alphabet : ')
    message, correct, error, word,arrCorrectWord = checkInput(userInput,myWord,word,arrCorrectWord)
    print(message)
    myError += error
    myCorrect += correct
    